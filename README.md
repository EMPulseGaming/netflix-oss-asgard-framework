# ![logo](https://goo.gl/f4aqsL)

## DESCRIPTION

The project 'aurora' is developed using 'Netflix OSS - Asgard' framework    
to manage OpenStack based Cloud across multiple-datacenters and regions.      
Currently, it is supporting Folsom and Grizzly releases of OpenStack and    
using APIs instead of CLI for services interaction. 

## Requirements

Grab the [latest standalone jar release](https://ebay.box.com/s/4elhlhfvdce1t5sq2lm2)    
Grab the [latest war release](https://ebay.box.com/s/c29kypl21q7tj62n3czw)

## Install

[running-aurora wiki](https://github.com/paypal/aurora/wiki/Running-aurora)

## Reporting issues

Issues Tracker is closed, this is deprecaited source, when ive time i'll update it.

## Submitting fixes

Fixes are submitted as pull requests via [BitBucket](https://bitbucket.org/EMPulseGaming/netflix-oss-asgard-framework/pull-requests/). 

## Copyright

License: Apache License Version 2.0

Read file [LICENSE](LICENSE.md)

## Links

[netflix io](http://netflix.github.io/)
[Netflix Open Source Platform - repo main page](https://github.com/Netflix)